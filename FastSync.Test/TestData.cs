﻿using System;

namespace FastSync.Test
{
	public class TestData
	{
		[SOPrimaryKey("Id")]
		public string Id{ get; set;}

		[SOField("Data1")]
		public string Data1 {get;set;}

		[SOField("Data2")]
		public string Data2 { get; set;}

		[SOField("Field3")]
		public string Field3;
	}
}

