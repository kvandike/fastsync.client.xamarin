﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FastSync
{
	public class SyncObject : IEnumerable<KeyValuePair<string,object>>
	{
		readonly string _reflectionClass;
		readonly object _id;
		IDictionary<string,object> items;
		internal readonly object mutex = new object();




		internal IDictionary<string, object> Items {
			get {
				return items ?? (items = new Dictionary<string,object> ());
			}
		}

		public SyncObject (string className, object id = null)
		{
			_reflectionClass = className;
			_id = id;
		}


		public SyncUser ACL { get; set;}

		#region IEnumerable implementation

		public IEnumerator<KeyValuePair<string, object>> GetEnumerator ()
		{
			return Items.GetEnumerator ();
		}

		#endregion

		#region IEnumerable implementation

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator ()
		{
			return Items.GetEnumerator ();
		}

		public object this [string key] {
			get {
				return Items [key];
			}
			set {
				Items [key] = value;
			}
		}

		#endregion

	

	}

	public class SyncObject<T> where T: class{

		readonly SyncObject obj;

		public SyncObject(T data){
			object id = null;
			bool isExistId = false;
			var typeInfo = data.GetType ().GetTypeInfo ();
			var idProp = typeInfo.DeclaredProperties.FirstOrDefault (x => x.GetCustomAttribute<SOPrimaryKey> ()!=null);
			if (idProp != null) {
				id = idProp.GetValue (data);
				isExistId = true;
			}
			if (id == null) {
				var idField = typeInfo.DeclaredFields.FirstOrDefault (x => x.GetCustomAttribute<SOPrimaryKey> ()!=null);
				if (idField != null) {
					id = idField.GetValue (data);
					isExistId = true;
				}
			}
			if (!isExistId) {
				throw new NullReferenceException ("primary key is null, please, add attribute SOPrimaryKey");
			}
			obj = new SyncObject (typeInfo.Name, id);

			foreach (var prop in typeInfo.DeclaredProperties.Where (x=>x.GetCustomAttribute <SOField>()!=null)) {
				var attr = prop.GetCustomAttribute<SOField> ();
				obj [attr.Name] = prop.GetValue (data);
			}

			foreach (var field in typeInfo.DeclaredFields.Where (x=>x.GetCustomAttribute <SOField>()!=null)) {
				var attr = field.GetCustomAttribute<SOField> ();
				obj [attr.Name] = field.GetValue (data);
			}
		}

	}
}

