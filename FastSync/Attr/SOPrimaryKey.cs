﻿using System;

namespace FastSync
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class SOPrimaryKey : Attribute
	{
		public string Name { get; set; }

		public SOPrimaryKey (string name)
		{
			Name = name;
		}
			
	}
}

