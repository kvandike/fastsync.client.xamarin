﻿using System;

namespace FastSync
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class SOField : Attribute
	{
		public string Name { get; set; }

		public SOField (string name)
		{
			Name = name;
		}
	}
}

