﻿using System;
using SQLite.Net.Interop;

namespace FastSync
{
	public class SyncAppFactory
	{

		CacheHelper _cacheHelper;
		RestHelper _restHelper;

		public SyncAppFactory SetCache(ISQLitePlatform platform, string filepath){
			_cacheHelper = new CacheHelper (platform, filepath);
			return this;
		}


		public SyncApp Build(){
			return new SyncApp (_restHelper, _cacheHelper);
		}


	}
}

