﻿using System;
using System.Threading.Tasks;

namespace FastSync
{
	public class SyncApp
	{
		RestHelper _restHelper;
		CacheHelper _cacheHelper;

		internal SyncApp (RestHelper restHelper, CacheHelper cacheHelper)
		{
			_restHelper = restHelper;
			_cacheHelper = cacheHelper;
		}

		public async Task Sync(){
		}

		public async Task Save(SyncObject obj){
		}

		public async Task Save<T>(SyncObject<T> obj){
			
		}

		public async Task Delete(SyncObject obj){
		}

		public async Task Delete<T>(SyncObject<T> obj){

		}


	}
}

